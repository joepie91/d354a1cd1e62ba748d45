  static login(email, password) {
    return Promise.try(() => {
      return Account.fetch(email))
    }).then((acc) => {
      return Promise.try(() => {
        bcrypt.compareAsync(password, acc.password)
      }).then(result => {
        if (result) {
          console.log(`User logged in: ${acc.email}`);
          const token = jwt.sign(_.pick(acc, ['id', 'email', 'agencyId']),
            process.env.JWT_SECRET, {expiresIn: 1440 * 60 * 11});
          return {token};
        } else {
          throw new Error('Onjuist wachtwoord');
        }
      })
    });
  }